/*Ответы на теоретические вопросы:
1. Существуют такие типы данных js:
        Undefined (Неопределённый тип)
        Boolean (Логический тип)
        Number (Число)
        String (Строка)
        BigInt (Позволяет работать с целыми числами произвольной длины)
        Symbol (Создание уникальных индентификаторов)
        Null (Отсутствует какое либо значение)
        Function (Объектный тип)
        Object (Набор данных и структур)
2. Оператор == приводит значения к одному типу
    а оператор === проверяет на полную идентичность
3. Оператор - функция которая выполняет действия с операндами например + - / * и тд и возвращает значение.
    */

let name = prompt('Введите свое имя');
let age = +prompt('Введите ваш возраст');
let exam;

while (!name) {
    name = prompt('Введите свое имя');
}

while (!age) {
    age = +prompt('Введите ваш возраст');
}

if (age < 18) {
    alert('You are not allowed to visit this website');
}
else if (age > 22) {
    alert('Welcome, ' + name);
}
else if (age >= 18 && age <= 22) {
    exam = confirm('Are you sure you want to continue?');
    if (exam) {
        alert('Welcome, ' + name);
    }
    else {
        alert('You are not allowed to visit this website');
    }
}


